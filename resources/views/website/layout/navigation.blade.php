<nav class="navbar navbar-expand-md bg-dark">
    <div class="container">
        <!-- Brand -->
        <a class="navbar-brand text-white" href="/"><span>Logo</span> Here</a>

        <!-- Navbar links -->
        <div>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-white" href="/auth/sign-in">登入</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="/auth/sign-up">註冊</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
