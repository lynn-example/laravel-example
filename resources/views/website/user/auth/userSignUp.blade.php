@extends('website.layout.master')

@section('title', '測試')

@section('content')
    <div id="signUpContainer"></div>
@endsection

@section('css')
    <link rel='stylesheet' href='/assets/svelte/website/auth/sign-up/bundle.css?v={{ \Carbon\Carbon::now()->format("YmdHis") }}'>
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/axios@0.19.2/dist/axios.min.js"></script>
    <script defer src='/assets/svelte/website/auth/sign-up/bundle.js?v={{ \Carbon\Carbon::now()->format("YmdHis") }}'></script>
@endsection
