<?php

return [
    // 登入
    'sign_in'    => [
        'name'                           => '登入',
        'description'                    => 'Let\'s get you in your account',
        'sign_in_with_facebook'          => 'Facebook 登入',
        'sign_in_with_line'              => 'Line 登入',
        'user_can_not_login'             => '帳號無法登入',
        'user_password_not_match'        => '帳號或密碼錯誤',
        // 'user_phone_already_registered'  => '手機已註冊過',
        'user_sign_in_already'           => '使用者已登入',
        'user_number_already_registered' => '使用者編號已重複，請再重新註冊一次',
    ],
    'sign_out'   => [
        'name' => '登出',
    ],
    // 註冊
    'sign_up'    => [
        'name'                          => '註冊',
        'already_have_account_sign_in'  => '已有帳號？點選這裡登入',
        'user_phone_already_registered' => '手機已註冊過',
        'user_email_already_registered' => 'Email 已註冊過',
        'user_password_is_invalid'      => '密碼與確認密碼不一致',
    ],
    // 帳號
    'account'    => [
        'create'                          => '建立帳號',
        'user_not_found'                  => '帳號或密碼錯誤',
        'user_not_found_by_serial_number' => '找不到此會員序號',
    ],
    // 密碼
    'password'   => [
        'forgot' => '忘記密碼？',
    ],
    'permission' => [
        'sign_in_permission_deny' => 'Permission deny 無法登入',
    ],
];
