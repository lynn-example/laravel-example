<?php

return [
    'profile'     => [
        'phone'                 => '手機號碼',
        'password'              => '密碼',
        'password_confirmation' => '確認密碼',
        'name'                  => '姓名',
        'email'                 => 'Email',
        'birthday'              => '生日',
        'gender'                => '性別',
        'status'                => '帳號狀態',
        'last_login_at'         => '上次登入時間',
        'created_at'            => '註冊時間',
        'old_password'          => '舊密碼',
        'new_password'          => '新密碼',
        'user_name'             => '使用者姓名',
        'signup_source'         => '註冊來源',
        // 'tag'                   => '標籤',
    ],
    'password'    => [
        'old_password'                               => '舊密碼',
        'new_password'                               => '新密碼',
        'new_password_confirmation'                  => '確認密碼',
        'update_password'                            => '更新密碼',
        'new_password_can_not_same_as_before'        => '密碼不可與之前相同',
        'password_not_same_as_password_confirmation' => '密碼與確認密碼不同',
        'old_password_not_match'                     => '舊密碼輸入錯誤',
        'old_password_is_required'                   => '必須輸入舊密碼',
    ],
    'basic'       => [
        'name' => '使用者',
    ],
    'status'      => [
        'user_not_login' => '使用者尚未登入',
    ],
    // 'validation'  => [
    //     'user_phone_is_already_used'          => '此電話已被使用',
    //     'user_profile_is_lock_can_not_update' => '資料已鎖定，無法更新',
    //     'user_profile_update_permission_deny' => '無法更新，權限不足',
    //     'user_social_is_already_used'         => '此第三方帳號已被其他帳號綁定，無法綁定',
    //     'user_already_connect_social'         => '已綁定',
    // ],
    'member'      => [
        'name' => '會員',
    ],
    'create'      => [
        'name'                => '建立帳號',
        'create_user_profile' => '建立帳號',
    ],
    'edit'        => [
        'name'                          => '編輯會員資料',
        'update_user_profile'           => '更新會員資料',
        'edit_password'                 => '變更密碼',
        'edit_user_password'            => '變更會員密碼',
        'personal_profile'              => '個人資料',
        'personal_password'             => '密碼更新',
        'profile_integrity'             => '個人資料完整度',
        'profile_integrity_description' => '必須完成個人資料才可以使用完整的服務',
        'update_profile_name'           => '編輯個人資料',
        'update_personal_profile'       => '更新個人資料',
        'update_personal_password'      => '更新個人密碼',
    ],
    'memo'        => [
        'profile' => [
            'description' => '備註',
            'created_at'  => '建立時間',
            'updated_at'  => '更新時間',
        ],
        'manage'  => [
            'name'                        => '管理車主備註',
            'search_user_car_placeholder' => '輸入車主備註資料進行搜尋',
            'user_memo_list'              => ':user_name 車主備註列表',
        ],
        'create'  => [
            'name'                  => '建立車主備註',
            'create_user_memo_name' => '建立 :user_name 車主備註',
            'create_user_memo'      => '建立備註',
        ],
        'edit'    => [
            'name'                => '編輯車主備註',
            'edit_user_memo_name' => '編輯 :user_name 車主備註',
            'update_user_memo'    => '更新車輛',
        ],
    ],
];
