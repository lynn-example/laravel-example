import App from './App.svelte';

const app = new App({
	target: document.getElementById('signUpContainer'),
	props: {
		name: '註冊頁'
	}
});

export default app;