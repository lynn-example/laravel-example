    const PUBLIC_ROOT = '../../../public';
    const CONTROLLER_WEBSITE_ROOT = '../Controller/website';
    const CONTROLLER_ADMIN_ROOT = '../Controller/admin';

    export const SVELTE_ROUTE = {
    	WEBSITE: {
            DEFAULT: {
                INPUT_SRC: CONTROLLER_WEBSITE_ROOT + '/test/src/main.js',
                OUTPUT_JS: PUBLIC_ROOT + '/assets/svelte/test/bundle.js',
                OUTPUT_CSS: PUBLIC_ROOT + '/assets/svelte/test/bundle.css',
            },
    		SIGN_IN: {
	    		INPUT_SRC: CONTROLLER_WEBSITE_ROOT + '/auth/sign-in/src/main.js',
	    		OUTPUT_JS: PUBLIC_ROOT + '/assets/svelte/website/user/auth/sign-in/bundle.js',
	    		OUTPUT_CSS: PUBLIC_ROOT + '/assets/svelte/website/user/auth/sign-in/bundle.css',
    		},
    		SIGN_UP: {
    			INPUT_SRC: CONTROLLER_WEBSITE_ROOT + '/auth/sign-up/src/main.js',
	    		OUTPUT_JS: PUBLIC_ROOT + '/assets/svelte/website/user/auth/sign-up/bundle.js',
	    		OUTPUT_CSS: PUBLIC_ROOT + '/assets/svelte/website/user/auth/sign-up/bundle.css',
    		}
    	},
    	ADMIN: {
            DEFAULT: {
                INPUT_SRC: CONTROLLER_ADMIN_ROOT + '/test/src/main.js',
                OUTPUT_JS: PUBLIC_ROOT + '/assets/svelte/test/bundle.js',
                OUTPUT_CSS: PUBLIC_ROOT + '/assets/svelte/test/bundle.css',
            },
    	}
    };