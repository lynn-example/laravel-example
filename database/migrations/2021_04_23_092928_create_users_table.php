<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 20)->comment('會員流水號');                        // 會員流水號
            $table->string('email', 255)->nullable()->comment('email');            // email
            $table->string('password', 60)->nullable()->comment('密碼');            // 密碼
            $table->string('status', 10)->comment('帳號狀態');                      // 帳號狀態（待驗證、正常、停權）
            $table->timestamp('last_login_at')->nullable()->comment('最後登入時間');        // 最後登入時間（驗證是否為活躍會員）
            $table->tinyInteger('email_verify_status')->comment('Email 驗證狀態');         // Email 驗證狀態
            $table->timestamp('email_verified_at')->nullable()->comment('Email 驗證時間'); // Email 驗證時間
            $table->timestamps();   // 時間戳記

            // 索引
            $table->primary(['id'], 'user_id_pk');
            $table->index(['email'], 'user_email_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
