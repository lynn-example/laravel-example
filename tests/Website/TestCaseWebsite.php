<?php
namespace Tests\Website;

use App\CoreModule\Environment\Support\EnvironmentSupport;
use Tests\TestCase;

class TestCaseWebsite extends TestCase {

    function setUp(): void
    {
        parent::setUp();
        // 設定網址
        $hosts = EnvironmentSupport::getEnvironmentWebsiteHosts();
        
        $app_url= "http://{$hosts}";
        config(['app.url' => $app_url]);

        \URL::forceRootUrl($app_url);
    }
}
