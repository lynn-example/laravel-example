<?php

namespace Tests\Website\User;

use Tests\Website\TestCaseWebsite;

class UserAuthTest extends TestCaseWebsite {
    /**
     *  註冊
     */
    public function test_SignUp()
    {
        $uri = '/auth/sign-up';
        $parameter = [
            'email'   => 'test004@example.com',
            'password'  => 'password',
            'password_confirmation' => 'password',
        ];

        // dump($parameter);
        $response = $this->json('POST', $uri, $parameter);
        $content = $response->json();
        dump($content);
        exit;
    }    
}
