<?php

use App\ExampleModule\Core\Constant\Middleware\MiddlewareConstant;
use App\ExampleModule\Core\Constant\Route\WebSiteRouteConstant;

Route::group([], function() {
    // 不需登入
    Route::group(['prefix' => 'auth'], function() {
    	Route::get('/sign-up', 'Website\User\Auth\UserAuthController@userAuthSignUpPage')->name(WebSiteRouteConstant::WEBSITE_SIGN_UP_PAGE);  // 註冊頁面
        Route::post('/sign-up', 'Website\User\Auth\UserAuthController@userAuthSignUpProcess')->name(WebSiteRouteConstant::WEBSITE_SIGN_UP_PROCESS);  // 註冊處理
    });
});


