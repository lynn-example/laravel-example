<?php

use App\ExampleModule\Core\Constant\Middleware\MiddlewareConstant;
use App\ExampleModule\Core\Constant\Route\WebSiteRouteConstant;

Route::group([], function() {
    Route::get('/', 'Website\Main\MainController@showMainPage')->name(WebSiteRouteConstant::WEBSITE_MAIN_PAGE);     // 前台首頁
});


