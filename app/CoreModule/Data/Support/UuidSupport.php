<?php

namespace App\CoreModule\Data\Support;


class UuidSupport {
    /**
     * 產生包含表頭隨機字串
     * @author  KJ  kejyun@gmail.com
     */
    public static function generateRandomStringWithPrefix($header = '', $str_length = 20, $candidate_text = 'abcdefghijklmnopqrstuvwxyz1234567890')
    {
        // 隨機字串長度
        $rand_str_length = $str_length - strlen($header);

        return $header . static::generateRandomString($rand_str_length, $candidate_text);
    }

    /**
     * 產生附加表頭隨機字串
     * @author  KJ  kejyun@gmail.com
     */
    public static function generateRandomStringAppendPrefix($prefix = '', $string_length = 10, $connect_symble = '-', $candidate_text = 'abcdefghijklmnopqrstuvwxyz1234567890')
    {
        return $prefix . $connect_symble . static::generateRandomString($string_length, $candidate_text);
    }

    /**
     * 產生隨機字串
     * @author  KJ  kejyun@gmail.com
     */
    public static function generateRandomString($rand_str_length, $candidate_text = 'abcdefghijklmnopqrstuvwxyz1234567890')
    {
        // 亂數字串
        $rand_str = '';
        // 候選文字長度
        $candidate_text_length = strlen($candidate_text)-1;

        for ($i = 0; $i < $rand_str_length; ++$i) {
            // 隨機挑選文字
            $rand_str .= $candidate_text[mt_rand(0, $candidate_text_length)];
        }

        return $rand_str;
    }
}
