<?php

namespace App\CoreModule\Data\Support;


use File;

class StringSupport {
    protected static $HTML_ENTITIES_URL_IGNORE = [
        '&amp;' => '&'
    ];

    const WORD_CONVERT = [
        ',' => '&#44;',
        ':' => '&#58;',
    ];

    /**
     * 轉換文字特殊字元
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function convertWord(string $word)
    {
        $search = array_keys(static::WORD_CONVERT);
        $replace = array_values(static::WORD_CONVERT);
        $word = str_replace($search, $replace, $word);

        return $word;
    }

    /**
     * 中文字 JSON ENCODE
     *
     * @param      array    $data   轉換的資料
     *
     * @return     string   $json   JSON 資料
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function jsonEncodeChinese(array $data)
    {
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return $json;
    }

    /**
     * 陣列 JSON DECODE
     *
     * @param      string   $json   JSON 資料
     *
     * @return     array    $array  陣列資料
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function jsonDecodeArray(string $json)
    {
        $array = json_decode($json, true);
        return $array;
    }

    /**
     * 解析網址名稱
     *
     * @param      string   $json   JSON 資料
     *
     * @return     array    $array  陣列資料
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function htmlEntitiesUrl(string $url)
    {
        $search = array_keys(static::$HTML_ENTITIES_URL_IGNORE);
        $replace = array_values(static::$HTML_ENTITIES_URL_IGNORE);
        $url = htmlentities($url);
        $url = str_replace($search, $replace, $url);

        return $url;
    }

    /**
     * 解析字串為陣列
     *
     * @param      string   $delimiter      拆解字元
     * @param      string   $string         拆解字串
     *
     * @return     array    $array          陣列資料
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function explode(string $delimiter, string $string, $is_keep_empty_string = true)
    {
        $array = explode($delimiter, $string);

        // 去除前後空白
        foreach ($array as $value) {
            $value = trim($value);
        }

        if (!$is_keep_empty_string) {
            // 不保留空字串
            foreach ($array as $key => $value) {
                if (!strlen($value)) {
                    unset($array[$key]);
                }
            }
        }

        return $array;
    }

    /**
     * Change HTML Entity to Xml Predefine Entity
     * @param $HTMLString
     *
     * @return      mixed
     *
     * @access      public
     * @author      Abel            abel@thenewslnes.com
     */
    public static function XmlCharacterParser(string $HTMLString)
    {
        // 移除 XML 無法解析特殊字元
        // https://stackoverflow.com/questions/12229572/php-generated-xml-shows-invalid-char-value-27-message
        $HTMLString = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', '', $HTMLString);
        $search = [
            '"',
            '&',
            "'",
            '<',
            '>',
        ];

        $replace = [
            '&quot;',
            '&amp;',
            '&apos;',
            '&lt;',
            '&gt;',
        ];
        return str_replace($search, $replace, $HTMLString);
    }

    /**
     * 取得 JSON 檔案成陣列
     *
     * @param      string   $delimiter      拆解字元
     * @param      string   $string         拆解字串
     *
     * @return     array    $array          陣列資料
     *
     * @access     public
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function getJsonArrayFromFile(string $file_path)
    {
        $content_json = File::get($file_path);
        $content_array = static::jsonDecodeArray($content_json);
        return $content_array;
    }
}
