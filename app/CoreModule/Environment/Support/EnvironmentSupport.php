<?php

namespace App\CoreModule\Environment\Support;

class EnvironmentSupport {
    /**
     * 取得環境網址
     *
     * @author     KeJyun      kejyun@gmail.com
     */
    public static function getEnvironmentWebsiteHosts()
    {
        // 取得 Env 網址設定
        $web_hosts = env('WEBSITE_HOSTS');

        if (is_null($web_hosts)) {
            // 沒有設定不分網址，使用當前網址
            $web_hosts = request()->getHttpHost();
        }

        return $web_hosts;
    }
}