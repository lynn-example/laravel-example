<?php

namespace App\CoreModule\Infrastructure\Repository;


use Illuminate\Database\Eloquent\Model;

abstract class CoreRepository {
    protected $Entity;
    /**
     * 建構子
     */
    public function __construct()
    {
    }

    /**
     * 建立資料
     * @param array $entity_data
     * @return |null
     */
    public function createEntity(array $entity_data)
    {
        $Entity = null;
        if ($this->Entity instanceof Model) {
            $Entity = $this->Entity->create($entity_data);
        }
        return $Entity;
    }
}
