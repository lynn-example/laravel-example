<?php

namespace App\Http\Controllers\Website\User\Auth;

use App\ExampleModule\Core\Constant\Route\WebSiteRouteConstant;
use App\ExampleModule\User\Constant\UserValidatorConstant;
use App\ExampleModule\User\Service\UserAuthService;
use App\ExampleModule\User\Validator\UserAuthValidator;
use App\Http\Controllers\Controller;
use Prettus\Validator\Exceptions\ValidatorException;
use Arr;
use DB;
use Exception;
use Log;


class UserAuthController extends Controller {

    protected $UserAuthService;
    protected $UserAuthValidator;

    public function __construct(
        UserAuthService $UserAuthService,
        UserAuthValidator $UserAuthValidator
    )
    {
        $this->UserAuthService = $UserAuthService;
        $this->UserAuthValidator = $UserAuthValidator;

        parent::__construct();
    }

    public function userAuthSignUpPage()
    {
        $binding = [];

        return view('website.user.auth.userSignUp', $binding);
    }

    public function userAuthSignUpProcess()
    {
        try {
            $allow_input = [
                'email',
                'password',
                'password_confirmation',
            ];
            $input = request()->all($allow_input);

            // 驗證資料
            $this->UserAuthValidator
                ->with($input)
                ->passesOrFail(UserValidatorConstant::WEBSITE_SIGN_UP);

            DB::beginTransaction();

            // 註冊來源
            $User = $this->UserAuthService->websiteCreateNormalUser($input);

            DB::commit();

            // 登入使用者
            auth()->login($User);

            // 抓取是否有想嘗試登入的頁面
            $intend_url = redirect()->intended()->getTargetUrl();
            $redirect_url = route(WebSiteRouteConstant::WEBSITE_MAIN_PAGE);
            if (!is_null($intend_url)) {
                // 若有嘗試登入的頁面，重新導向
                $redirect_url = $intend_url;
            }

            // 回應資料
            $data = [
                'url'  => [
                    'home_page_url' => $redirect_url,
                ],
            ];

            $response_data = [
                'status' => 'success',
                'data'   => $data,
            ];

            return response()->json($response_data, 200, [], JSON_UNESCAPED_UNICODE);
        } catch (Exception $exception) {
            Log::error($exception);

            $error_message = $exception->getMessage();
            if ($exception instanceof ValidatorException) {
                $error_message = $exception->getMessageBag();
            }

            $response_data = [
                'status' => 'failure',
                'error' => [
                    'code'=> $exception->getCode(),
                    'message'=> $error_message,
                ],
            ];

            return response()->json($response_data, 200, [], JSON_UNESCAPED_UNICODE);
        }
    }
}
