<?php

namespace App\Http\Controllers\Website\Main;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MainController extends Controller {

    public function __construct()
    {
        parent::__construct();
    }
   
    public function showMainPage()
    {
		$binding = [];

		return view('website.main.mainHome', $binding);
    }
}
