<?php

namespace App\ExampleModule\Core\Constant\Route;


class WebSiteRouteConstant {
	// 前台
    const WEBSITE_MAIN_PAGE = 'WEBSITE_MAIN_PAGE';
    const WEBSITE_SIGN_UP_PAGE = 'WEBSITE_SIGN_UP_PAGE';
    const WEBSITE_SIGN_UP_PROCESS = 'WEBSITE_SIGN_UP_PROCESS';
}
