<?php

namespace App\ExampleModule\Core\Constant\Middleware;


class MiddlewareConstant {
    const WEBSITE_AUTH_GUEST = 'website.auth.guest';              // 未登入，沒有任何 Session，可以通過檢查
    const WEBSITE_AUTH_VERIFY = 'website.auth.verify';            // 已登入、帳號已完成，可以通過檢查
    const WEB_GROUP = 'web';    // Web 群組
}
