<?php

namespace App\ExampleModule\User\Repository;

use App\CoreModule\Infrastructure\Repository\CoreRepository;
use App\ExampleModule\ExceptionCode\User\UserExceptionCode;
use App\ExampleModule\User\Entity\User;
use Carbon\Carbon;
use Exception;

class UserRepository extends CoreRepository {
    protected $User;
    public function __construct(
        User $User
    ) {
        $this->User = $User;
        $this->Entity = $User;
        parent::__construct();
    }

    /**
     * 更新使用者最後登入時間
     * @param string $user_id
     * @return mixed
     * @throws Exception
     */
    public function updateUserLastLoginAt(string $user_id)
    {
        // 撈取使用者
        $User = $this->User
            ->where('id', $user_id)
            // ->setLockQuery()
            ->first();

        if (is_null($User)) {
            throw new Exception(
                __('auth.account.user_not_found'),
                UserExceptionCode::USER_NOT_FOUND
            );
        }

        // 更新登入時間
        $User->last_login_at = Carbon::now();
        $update_result = $User->save();

        return $update_result;
    }

    /**
     * 新增使用者註冊資料
     * @param array $user_data
     * @return mixed
     * @throws Exception
     */
    public function createUser(array $user_data)
    {
        $User = $this->User->create($user_data);

        return $User;
    }

    /**
     * 透過 Email 尋找使用者
     *
     * @param string $email
     * @return mixed
     */
    public function findUserByEmail(string $email)
    {
        // 抓取資料
        $User = $this->User
            ->where('email', $email)
            // ->setLockQuery()
            ->first();

        return $User;
    }
}
