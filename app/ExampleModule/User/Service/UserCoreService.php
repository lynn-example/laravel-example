<?php

namespace App\ExampleModule\User\Service;


use App\CoreModule\ExceptionCode\User\UserExceptionCode;
use App\CoreModule\Infrastructure\Service\CoreService;
use App\ExampleModule\User\Repository\UserRepository;
use Exception;

class UserCoreService extends CoreService {
    protected $UserRepository;
    public function __construct(
        UserRepository $UserRepository
    ) {
        $this->UserRepository = $UserRepository;
        parent::__construct();
    }

    /**
     * 透過Email尋找使用者
     *
     * @param   $email
     * @return  mixed
     * @throws  Exception
     */
    public function findUserByEmail($email)
    {
        $User = $this->UserRepository->findUserByEmail($email);
        $User = Arr::get($User, 'User');

        return $User;
    }

    /**
     * 透過Email尋找使用者
     *
     * @param $email
     * @return mixed
     * @throws Exception
     */
    public function findOrFailUserByEmail($email)
    {
        // 透過電話找使用者
        $User = $this->findUserByEmail($email);

        if (is_null($User)) {
            throw new Exception(
                __('auth.account.user_not_found'),
                UserExceptionCode::USER_NOT_FOUND
            );
        }

        return $User;
    }
}
