<?php

namespace App\ExampleModule\User\Service;


use App\CoreModule\Infrastructure\Service\CoreService;
use App\ExampleModule\Core\ExceptionCode\User\UserExceptionCode;
use App\ExampleModule\User\Constant\UserStatusConstant;
use App\ExampleModule\User\Repository\UserRepository;
use App\ExampleModule\User\Constant\UserConstant;
use App\ExampleModule\User\Support\UserSupport;
use App\ExampleModule\User\Entity\User;
use Arr;
use Exception;
use Hash;
use Str;

class UserAuthService extends UserCoreService {
    protected $UserRepository;
    public function __construct(
        UserRepository $UserRepository
    ) {
        $this->UserRepository = $UserRepository;
        parent::__construct(
            $UserRepository
        );
    }

    /**
     * 前台註冊
     * @param   $user_data
     * @throws  Exception
     */
    public function websiteCreateNormalUser(array $user_data)
    {
        // 過濾資料
        $allow_input = [
            'email',
            'password',
            'password_confirmation',
        ];

        // 註冊驗證
        $user_data = Arr::only($user_data, $allow_input);
        $email = Arr::get($user_data, 'email');
        $password = Arr::get($user_data, 'password');
        $password_confirmation = Arr::get($user_data, 'password_confirmation');

        // 檢查密碼是否可以相同
        if ($password != $password_confirmation) {
            throw new Exception(
                __('auth.sign_up.user_password_is_invalid'),
                UserExceptionCode::USER_PASSWORD_IS_INVALID
            );
        }

        $user_data['status'] = UserStatusConstant::STATUS_UNDONE;   // 資料未完成

        $User = $this->findUserByEmail($email);

        if (!is_null($User)) {
            throw new Exception(
                __('auth.sign_up.user_email_already_registered'),
                UserExceptionCode::USER_EMAIL_ALREADY_REGISTERED
            );
        }

        // 會員資料
        $user_data = $user_data + UserConstant::DEFAULT_USER_VALUE;
        $user_data['last_login_at'] = date('Y-m-d H:i:s');
        
        // 會員編號
        $user_data['id'] = UserSupport::generateUserId();

        // 加密密碼
        if (!is_null($user_data['password'])) {
            $user_data['password'] = \Hash::make($user_data['password']);
        }

        $User = $this->UserRepository->createUser($user_data);

        return $User;
    }

    /**
     * 透過 Email 尋找使用者
     *
     * @param   $email
     * @return  mixed
     * @throws  Exception
     */
    public function findUserByEmail($email)
    {
        $User = $this->UserRepository->findUserByEmail($email);

        return $User;
    }

    /**
     * 記錄最後登入時間
     *
     * @param   $user_id
     * @return  mixed
     * @throws  Exception
     */
    public function updateUserLastLoginAt($user_id)
    {
        $User = $this->UserRepository->updateUserLastLoginAt($user_id);

        return $User;
    }
}
