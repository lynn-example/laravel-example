<?php

namespace App\ExampleModule\User\Validator;

use App\ExampleModule\User\Constant\UserValidatorConstant;
use Illuminate\Validation\Rule;
use Prettus\Validator\LaravelValidator;

class UserAuthValidator extends LaravelValidator {
    protected $rules = [
        // 前台註冊
        UserValidatorConstant::WEBSITE_SIGN_UP => [
            'email'   => [
                'required',
                'email',
            ],
            'password'   => [
                'required',
                'min:8',
            ],
            'password_confirmation'   => [
                'required',
            ],
        ],
        // 前台登入
        UserValidatorConstant::WEBSITE_SIGN_IN => [
            'account'   => [
                'required',
            ],
            'password'   => [
                'required',
                'min:8',
            ],
        ],
    ];

    protected $attributes = [
        
    ];

    protected $messages = [
        // 'parent_id.numeric' => 'Parent id must be numeric',
        // 'name' => 'We need to know your e-mail address!',
    ];
}
