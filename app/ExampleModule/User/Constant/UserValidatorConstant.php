<?php

namespace App\ExampleModule\User\Constant;


class UserValidatorConstant {
    // 前台
    const WEBSITE_SIGN_UP = 'WEBSITE_SIGN_UP'; // 前台註冊
    const WEBSITE_SIGN_IN = 'WEBSITE_SIGN_IN';  // 前台登入
}
