<?php

namespace App\ExampleModule\User\Constant;

class UserEmailVerityStatusConstant
{
    const STATUS_NOT_VERIFY = 0;    // 未驗證
    const STATUS_VERIFY = 1;        // 已驗證
}
