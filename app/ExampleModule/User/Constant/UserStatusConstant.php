<?php

namespace App\ExampleModule\User\Constant;

class UserStatusConstant
{
    const STATUS_UNDONE = 'U';  // 待驗證
    const STATUS_ENABLE = 'E';  // 正常
    const STATUS_DISABLE = 'D'; // 停權
}
