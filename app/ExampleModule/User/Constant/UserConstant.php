<?php

namespace App\ExampleModule\User\Constant;


use App\ExampleModule\User\Constant\UserEmailVerityStatusConstant;
use App\ExampleModule\User\Constant\UserStatusConstant;

class UserConstant {
    const ID_PREFIX = 'usr';  // 編號前綴
    const ID_LENGTH = 15;   // 編號長度

    // 會員預設資料
    const DEFAULT_USER_VALUE = [
        'email'                      => null,
        'password'                   => null,
        'status'                     => UserStatusConstant::STATUS_UNDONE,
        'last_login_at'              => null,
        'email_verify_status'        => UserEmailVerityStatusConstant::STATUS_NOT_VERIFY,
    ];
}
