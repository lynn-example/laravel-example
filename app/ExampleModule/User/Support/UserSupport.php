<?php

namespace App\ExampleModule\User\Support;

use App\CoreModule\Data\Support\UuidSupport;
use App\ExampleModule\User\Constant\UserConstant;
use App\ExampleModule\User\Entity\User;

class UserSupport {


    /**
     * 產生使用者編號
     * @author  KJ  kejyun@gmail.com
     */
    public static function generateUserId()
    {
        $id_prefix = UserConstant::ID_PREFIX;
        $id_length = UserConstant::ID_LENGTH;
        $id = UuidSupport::generateRandomStringAppendPrefix($id_prefix, $id_length);
        return $id;
    }
    
    /**
     * 產生系統登入
     * @author  KJ  kejyun@gmail.com
     */
    public static function getMockUser()
    {
        $MockUser = new User;
        $MockUser->id = null;

        return $MockUser;
    }
}
